class AppointmentsController < ApplicationController
    def index
        @appointments = Appointment.select("*").order('created_at ASC')
    end
    
    def new
        @appointment = Appointment.new
    end
    
    def show
        @appointments = Appointment.find(params[:id])
    end

    def create
    @appointments = Appointment.new(appointment_params)
        if @appointments.save
            puts @appointments
            flash[:success]= 'Appointment was successfully created.'
            if !@appointments.time.nil?
                redirect_to '/home?appt=' + @appointments.kind.to_s + '&apptDate=' + @appointments.date.to_s + '&apptTime=' + @appointments.time.strftime('%I:%M%p')
            else
                redirect_to request.referrer + '?no_time=1'
            end
        else 
            flash[:error]= 'Appointment was not created.'
            render 'new'
        end
    end
    
  def destroy
    @appointments = Appointment.find(params[:id])
        if @appointments.destroy
            flash[:success]= 'Appointment was successfully deleted.'
            redirect_to '/home'
        else 
            flash[:error]= 'Appointment was not deleted.'
            redirect_to '/home'
        end    
  end
  
  private
  def appointment_params
    params.require(:appointment).permit(:date, :time, :doctorid, :userid, :first_name, :last_name, :email, :kind)
  end
end
