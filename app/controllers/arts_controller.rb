class ArtsController < ApplicationController
    def index
      @arts = Art.all
    end
    
    def new
      @art = Art.new
    end
    
    def show
      @art = Art.find(params[:id])
      @appointment = Appointment.new
    end
    
    def edit
      @art = Art.find(params[:id])
    end
    
  def update
    @art = Art.find(params[:id])
    if @art.update(art_params)
      redirect_to "/arts"
    else
      redirect_to "/arts"
    end
  end

    def create
    @arts = Art.new(art_params)
        if @arts.save
            flash[:success]= 'Art was successfully created.'
            redirect_to '/arts/new'
        else 
            flash[:error]= 'Art was not created.'
            render 'new'
        end
    end
    
  def destroy
    @arts = Art.find(params[:id])
        if @arts.destroy
            flash[:success]= 'Art was successfully deleted.'
            redirect_to '/gallery'
        else 
            flash[:error]= 'Art was not deleted.'
            redirect_to '/gallery'
        end    
  end
  
  private
  def art_params
    params.require(:art).permit(:title, :artist, :link, :price)
  end
end
