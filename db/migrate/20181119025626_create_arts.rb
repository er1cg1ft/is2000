class CreateArts < ActiveRecord::Migration
  def change
    create_table :arts do |t|
      t.string :title
      t.string :artist
      t.float :price
      t.timestamps null: false
    end
  end
end
