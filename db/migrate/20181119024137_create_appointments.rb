class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.date :date
      t.time :time
      t.integer :doctorid
      t.integer :userid
      t.string :first_name
      t.string :last_name
      t.string :email
      t.integer :kind
      t.timestamps null: false
    end
  end
end
